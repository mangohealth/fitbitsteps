#!/usr/bin/env bash

echo "Uploading IPA and dSYM to HockeyApp"

if [[ "$BUDDYBUILD_BRANCH" =~ "master" ]]; then
  HOCKEYAPP_API_TOKEN=20bab2ed21c047738e8edff5493747a3
  HOCKEYAPP_APP_ID=fc29757bd05d48158b04c86c0bad3618

  cd $BUDDYBUILD_PRODUCT_DIR
  find . -name "*.dSYM" -print | zip /tmp/dsyms.zip -@
  curl -v -F "ipa=@$BUDDYBUILD_IPA_PATH" \
          -F "dsym=@/tmp/dsyms.zip" \
          -F "status=2" \
          -F "notify=0" \
          -F "notes=testing buddybuild integrations" \
          -F "notes_type=0" \
          -H "X-HockeyAppToken: $HOCKEYAPP_API_TOKEN" \
       https://rink.hockeyapp.net/api/2/apps/$HOCKEYAPP_APP_ID/app_versions/upload
else
    echo "This wasn't a release branch!"
fi 
